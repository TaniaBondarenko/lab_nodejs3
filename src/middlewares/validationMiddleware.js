const Joi = require('joi');

const registrationValidator = async (req, res, next) => {
    const schema = Joi.object({
        email: Joi.string()
            .email()
            .required(),
        password: Joi.string()
            .min(4)
            .max(15)
            .required(),
        role: Joi.string()
        .required(),
    });

    try {
        await schema.validateAsync(req.body);
        next();
    }
    catch (err) {
        next(err);
    }
};
/*
const truckCreationValidator = async (req, res, next) => {
    const schema = Joi.object({
        type:Joi.string()
        .pattern(new RegExp(/\b(SPRINTER) | (SMALL STRAIGHT) | (LARGE STRAIGHT)\b/)),
        //status:Joi.enum()
            
    });

    try {
        await schema.validateAsync(req.body);
        next();
    }
    catch (err) {
        next(err);
    }
};*/



module.exports = {
    registrationValidator
};
