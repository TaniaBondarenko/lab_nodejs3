const jwt = require('jsonwebtoken');
const { InvalidAuthentificationError } = require('../utils/errors');

const authMiddleware = (req, res, next) => {
    const { authorization } = req.headers;
    if (!authorization) {
        throw new InvalidAuthentificationError('No authorization header provided');
    }

    const [, jwt_token] = authorization.split(' ');
    if (!jwt_token) {
        throw new InvalidAuthentificationError("No token provided")
    }
    try {
        const tokenPayload = jwt.verify(jwt_token, "secret");
        console.log('tokenPayload',tokenPayload)
        req.user = {
            userId: tokenPayload._id,
            email: tokenPayload.email,
            role:tokenPayload.role
        };
        next();
    } catch (err) {
        throw new InvalidAuthentificationError('Authentification error');
    }
};

const checkPermissionMiddleware = (req, res, next) => {
    const { role } = req.user;
    if (role !== 'DRIVER') {
        throw new InvalidAuthentificationError('No permission for such actions');  
    }
    next();
}

module.exports = {
    authMiddleware,
    checkPermissionMiddleware
}