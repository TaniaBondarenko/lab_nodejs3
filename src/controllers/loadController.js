const express = require('express');
const router = express.Router();
const { asyncWrapper } = require('../utils/apiUtils');
const { InvalidAuthentificationError, InvalidRequestError } = require('../utils/errors');
const { addLoadToShipper, getLoadsByUserId, getActiveLoad, getLoadById, updateLoadData, setNextState } = require('../services/loadService');
const { Load } = require('../models/loadModel');

router.post('/', asyncWrapper(async (req, res) => {
    const { userId, role } = req.user;
    if (role==='SHIPPER') {
        await addLoadToShipper(req.body, userId);
        res.status(200).json({ message: "load created successfully" });
    }
    if (role === "DRIVER") {
        throw new InvalidAuthentificationError('No authorized for drivers')
    }
}));

router.get('/', asyncWrapper(async (req, res) => {
    const { userId} = req.user;
    let status = req.query.status;
    const loads = await getLoadsByUserId(userId, status);
    console.log({loads}.length)
        res.status(200).json({ loads });
}));

router.put('/:id', asyncWrapper(async (req, res) => {
     const { userId,role} = req.user;
    if (role === "SHIPPER") {
       
        const { id } = req.params;
        const data = req.body;
        
        console.log('data',data)
        await updateLoadData(id, userId, data);
    res.status(200).json({ "message": "Load details changed successfully" });}
}));

router.get('/active', asyncWrapper(async (req, res) => {
    const { userId,role } = req.user;
    if (role === "SHIPPER") {
        const load = await getActiveLoad(userId)
        res.status(200).json({load}); 
    }
    else {
        throw new InvalidAuthentificationError('No authorized for drivers')
    }
}))

router.get('/:id', asyncWrapper(async (req, res) => {
    const { userId } = req.user;
    const { id } = req.params;
    const load = await getLoadById(id, userId);
    if (!load) {
        throw new InvalidRequestError('No truck with such id found');
    }
    res.status(200).json({ load });
   
}));

router.patch('/active/state', asyncWrapper(async (req, res) => {
    const { userId, role } = req.user;
        const newState = await setNextState(userId);
        res.status(200).json({
            message: `Load state changed to ${newState}`
        }) 
}));

module.exports = {
    loadRouter:router
}