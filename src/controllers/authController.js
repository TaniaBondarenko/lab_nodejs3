const express = require('express');
const router = express.Router();
const { asyncWrapper } = require('../utils/apiUtils');
const { register, login } = require('../services/authService');
const { registrationValidator } = require('../middlewares/validationMiddleware');

router.post('/register', registrationValidator, asyncWrapper(async (req, res) => {
    const { email, password, role } = req.body;
    await register({ email, password,role });
    res.status(200).json({ message: "Profile created successfully" })
}));

router.post('/login', asyncWrapper(async (req, res) => {
    const { email, password } = req.body;
    const jwt_token = await login({ email, password });
    res.status(200).json({ jwt_token: jwt_token })
}));

module.exports = {
    authRouter:router
}
