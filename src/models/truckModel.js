const mongoose = require('mongoose');

const Truck = mongoose.model('Trucks',
    {
        userId: {
            type: mongoose.Schema.Types.ObjectId,

        },
        created_by: {
            type: mongoose.Schema.Types.ObjectId,
            required: true
        },
        assigned_to: {
            type: mongoose.Schema.Types.ObjectId,
            default: null
    },
    type: {
            type: String,
        enum: ["SPRINTER","SMALL STRAIGHT","LARGE STRAIGHT" ],
            required: true
        },
    dimensions:Object,
    status: {
        type: String,
        enum: ["OL", "IS"],
        default: "IS"
    },
    
    created_date: {
        type: Date,
        default: Date.now()
    }
});    
    
module.exports = {
        Truck
    }