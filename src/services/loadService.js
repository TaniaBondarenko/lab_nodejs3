const { Load } = require('../models/loadModel');
const { Truck } = require('../models/truckModel');

const addLoadToShipper = async (loadPayload, userId) => {
    const load = new Load({ ...loadPayload, userId });
    await load.save();
}


const getLoadsByUserId =async(userId, status) => {
    const load = await Load.findOne({ userId,status });
    if (!load) {
        return "No active loads"
    }
    return load;
}

const getActiveLoad = async (userId) => {
        const load = await Load.findOne({
        assigned_to: userId,
        status:'ASSIGNED'
    });
    if (!load) {
        return "No active loads"
    }
}

const getLoadById = async (loadId, userId) => {
    const load = await Load.findOne({ _id: loadId, userId });
    return load;
}

const updateLoadData = async (loadId, userId,data) => {
    const load = await Load.findOneAndUpdate({ _id: loadId,userId }, { $set: data }, {new:true});
return load;
}

const setNextState = async (userId) => {
    const loadStatus = [null, "NEW", "POSTED", "ASSIGNED", "SHIPPED"];
    const loadState = [null, "En route to Pick Up", "Arrived to Pick Up", "En route to delivery", "Arrived to delivery"];
    let load = await Load.findOne({ userId: userId });
    if (load === null) {
        console.log('no loads found')
    }
    let currentIndex;
    for (i = 0; i < loadStatus.length; i++) {
        if (loadStatus[i] === load.status) {
            currentIndex = i;
        }
    }
    if (currentIndex === loadStatus.lenght - 1) {
        load= await Load.findOneAndUpdate({ userId: userId }, { $set: { state: null, status: null } }, { new: true })
    } else {
        load= await Load.findOneAndUpdate({ userId: userId }, { $set: { state: loadState[currentIndex + 1], status: loadStatus[currentIndex + 1] } }, { new: true })
    }
    return load.state;
}
    
module.exports = {
    addLoadToShipper,
    getLoadsByUserId,
    getActiveLoad,
    getLoadById,
    updateLoadData,
    setNextState
}