const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const { User } = require('../models/userModel');
const { InvalidAuthentificationError } = require('../utils/errors');

const register = async ({ email, password,role }) => {
    const user = new User({
        email,
        password: await bcrypt.hash(password, 10),
        role
    });
    await user.save();
}

const login = async ({ email, password }) => {
    const user = await User.findOne({ email });
    if (!user) {
        throw new InvalidAuthentificationError('Invalid email or password');
    }
    if (!(await bcrypt.compare(password, user.password))) {
        throw new InvalidAuthentificationError('Invalid email or password');
    }
    const jwt_token = jwt.sign({ _id: user._id, email: user.email,role:user.role }, 'secret');
    return jwt_token;
}

module.exports ={
    register,
    login
}