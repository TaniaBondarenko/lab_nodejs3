class CommonHandlerError extends Error{
    constructor(message) {
        super(message);
        this.status = 500;
    }
}

class InvalidRequestError extends CommonHandlerError{
    constructor(message) {
        super(message);
        this.state = 400;
    }
}

class InvalidAuthentificationError extends CommonHandlerError{
    constructor(message) {
        super(message);
        this.status = 401;
    }
}

module.exports = {
    CommonHandlerError,
    InvalidAuthentificationError,
    InvalidRequestError
}
