const asyncWrapper = (callback) => {
    return (req, res, next) => {
        callback(req, res)
            .catch(next);
    }
}

const checkTruckDimentions = (type) => {
    const dimentions = {
    'SPRINTER': { length: 300, width: 250, height: 170, capacity: 1700 },
    'SMALL STRAIGHT': { length: 500, width: 250, height: 170, capacity: 2500 },
    'LARGE STRAIGHT': { length: 700, width: 350, height: 200, capacity: 4000 }
};
return dimentions[type];
}

module.exports = {
    asyncWrapper,
    checkTruckDimentions
}
